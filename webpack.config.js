const debug = process.env.NODE_ENV !== 'production';
const webpack = require('webpack');

module.exports = {
    mode: process.env.NODE_ENV || 'development',
    context: `${__dirname}/src`,
    devtool: debug ? 'inline-sourcemap' : null,
    entry: './js/index.tsx',
    output: {
        path: `${__dirname}/src`,
        filename: 'app.min.js'
    },
    resolve: {
        extensions: ['.js', '.json', '.ts', '.tsx']
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                query: {
                    presets: ['react', 'es2015', 'stage-1'],
                    plugins: ['react-html-attrs', 'transform-decorators-legacy', 'transform-class-properties']
                }
            },
            {
                test: /\.tsx?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'awesome-typescript-loader',
            },
            { test: /\.css$/, loader: "style-loader!css-loader" },
        ],
    },
    plugins: debug ? [] : [
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.optimize.UglifyJsPlugin({ mangle: false, sourceMap: false }),
        new webpack.ProvidePlugin({
            "React": "React"
        })
    ]
};